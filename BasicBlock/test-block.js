//alert('this is a test!!');

//wp.blocks.registerBlockType(a,b)
//a = namespace 
//b = {object}
//object = title, icon, category, attributes, edit, save

wp.blocks.registerBlockType('blocks/first-block',{
    title: 'Test Block', //Title that will be displayed in the user interface
    icon: 'hammer', //Choose an icon from wordpress dashicons [Optional]
    category: 'common', //Where your block will show up in the user interface. Common, layout, inbeds, formatting, widgets, 
    attributes: { //Describes what your inparams should be. 
        content:{type: 'string'},
        color:{type:'string'}
    },
    edit: function(props){ //Controls the editing screen the user sees in the user interface
        function updateContent(event){
            props.setAttributes({content: event.target.value})
        }

        return wp.element.createElement( //here you could use React instead of wp.element
            "div", 
            null, //here you can add a attribute to the element like so: { className: props.className }
            wp.element.createElement(
                "h3", 
                null, 
                "It's a me!"), 
            wp.element.createElement(
                "input", {type: "text", onChange: updateContent, value: props.attributes.content})
        );   //This is written in React. But to foolproof this for the future we use Wordpress own syntax which is wp.element.
    }, 
    save: function(props){ //Controls what html is actually put out into the post
        return wp.element.createElement(
            "h3",
            null,
            props.attributes.content
          );
    },
});
