<?php
/**
 * Plugin Name: Test Block - without Webpack
 * Description: Without webpack and Babel
 * Author: Ellinor
 * Version: 1.0.0
 */
function loadMyBlockFiles() {
  wp_enqueue_script(
    'my-super-unique-handle',
    plugins_url('test-block.js', __FILE__), //Other option for this is plugin_dir_url(__FILE__) . 'test-block.js',
    array('wp-blocks', 'wp-i18n', 'wp-editor'),
    true
  );
}
 
add_action('enqueue_block_editor_assets', 'loadMyBlockFiles');

/* 
enqueue_block_editior_assets
Fires after block assets have been enqueued for the editing interface.

loadMyBlockFiles
Loads the function we have created above
*/