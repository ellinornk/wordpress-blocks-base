## BasicBlock: Done in ES5
## ReactBlock: Done in ESNext (aka build with React using Babel and Webpack)

### Instructions for BasicBlock:
- download repo
- add the map you want to wp-content/plugins
- Save
- Go into plugins in your Wordpress and activate it

### Instructions for ReactBlock:
- download repo
- add the map you want to wp-content/plugins
- npm install
- npm run build
- Go into plugins in your Wordpress and activate it
