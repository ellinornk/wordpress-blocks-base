import './style.scss';

const {registerBlockType} = wp.blocks;
const { FormFileUpload } = wp.components;


registerBlockType('blocks/test-block-webpack',{
    title: 'Test Block Webpack',
    icon: 'smiley', //choose one from wordpress dashicons
    category: 'common', //where it will end up when you choose them in the editor
    attributes: { //basically works like state in react but we must define what type they are here
        content: {type: 'string'},
        img: {type:'string'},
    },

    edit ({ attributes, setAttributes}) { //What happens in the editor and what the admin sees
        function updateContent(e){
            setAttributes({content: e.target.value});
        }
        function updateImg(e){
            setAttributes({img: e.target.value});
        }
        return( 
            <div>
                <h3 className="header">This is in the editor</h3>
                <input type="text" value={attributes.content} onChange={updateContent}/>
                <p>{attributes.content}</p>
                <img src={attributes.img} width="500px" height="500px"></img>
                <input type= "text" value={attributes.img} onChange={updateImg} placeholder="Add Img"/>

                <FormFileUpload //one of wordpress own components
                    accept="image/*"
                    onChange={ () => console.log('new image') }
                    > Upload
                </FormFileUpload>
            </div>
        );
    },

    save ({attributes}) { //What it will look like on actual page
        return (
            <div>
                <h3>This is the front end</h3>
                <p>{attributes.content}</p>
                <img src={attributes.img}></img>
            </div>
        );
    }
})
