<?php
/**
 * Plugin Name: React Block
 * Description: A block built with webpack and babel
 * Author: Ellinor
 * Version: 1.0.0
 */


// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;



function loadTheFiles(){
    wp_enqueue_script(
        //A unique name for our block. Usually namespace/blockname
        'blocks/test-block-webpack',
        //path to our output file. index.build.js is created when we add webpack
        plugins_url('build/index.build.js', __FILE__),
        //What we want to use from Wordpress in our Block
        array('wp-blocks', 'wp-element', 'wp-editor', 'wp-components'),
        true
    );
}

add_action('enqueue_block_editor_assets', 'loadTheFiles');
//from this tutorial: http://jschof.com/gutenberg-blocks/gutenberg-blocks-made-easy/